# all_utils

Provides utils for other mods.

## API

### Positions

* **all_utils.get_full_pos(player)** - get full pos from Player object, return table (include pitch and yaw)
* **all_utils.set_full_pos(player, pos)** - set full pos (teleport) to Player object (pos need include pitch and yaw)
* **all_utils.is_empty(pos)** - check pos walkable, return boolean
* **all_utils.find_position(pos, posnear)** - find empty (walkable) pos, pos - table (include xyz), posnear - search radius in nodes (number), return tule - boolean (true if found), pos (table xyz)
* **all_utils.find_top_or_down_pos(pos, top, limit)** - find empty (walkable pos) in top or down near the floor, top - boolean (if true - search in top, false - search in bottom), limit - number limit of search dist, return tuple - boolean (true if found), pos (table xyz)

### Tables

* **all_utils.table_to_string(t)** return string *key = value* separate by newline (*\n*)
* **all_utils.table_to_string_keys(t)** - return string keys separate by comma
* **all_utils.table_to_string_values(t)** return string values separate by comma
* **all_utils.table_len(t)** - return number
* **all_utils.table_floor(t)** - floor values in table, return table
* **all_utils.table_equals(t, t2)** - check equals, return boolean
* **all_utils.table_keys(t)** - return keys array
* **all_utils.table_values(t)** - return values array
* **all_utils.table_split(t, num, named)** - return splitted array, num - parts length, named - save keys or not (bool)

### Time

* **all_utils.parse_time(str)** - parse time from string, using suffixes (s - seconds, m - minutes, h - hours, d - days), return seconds (number)
* **all_utils.get_time(sec)** - get human ready value, return tuple - string prefix, number value
