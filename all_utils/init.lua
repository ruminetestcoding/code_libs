all_utils = {}
local sep = package.path:match('(%p)%?%.')
local modpath = minetest.get_modpath(minetest.get_current_modname())

dofile(modpath .. sep .. 'modules' .. sep .. 'pos.lua')
dofile(modpath .. sep .. 'modules' .. sep .. 'tables.lua')
dofile(modpath .. sep .. 'modules' .. sep .. 'time.lua')
