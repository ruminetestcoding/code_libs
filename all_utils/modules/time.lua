function all_utils.parse_time(str) -- s - seconds, m - minutes, h - hours, d - days, return seconds
    local function parse(s)
        local e = s:sub(s:len()-1, s:len()):lower()
        if e == 's' then return tonumber(s:sub(0, s:len() - 1))
        elseif e == 'm' then
            local sec = tonumber(s:sub(0, s:len() - 1))
            if not sec then return nil end
            return sec * 60
        elseif e == 'h' then
            local sec = tonumber(s:sub(0, s:len() - 1))
            if not sec then return nil end
            return sec * 60 * 60
        elseif e == 'd' then
            local sec = tonumber(s:sub(0, s:len() - 1))
            if not sec then return nil end
            return sec * 60 * 60 * 24
        else return tonumber(s) end
    end

    local result = 0
    if str:find(' ') then
        local args = str:split(' ')
        for key, value in pairs(args) do
            local sec = parse(value)
            if sec then result = result + sec end
        end
    else result = parse(str) end
    return result
end

function all_utils.get_time(sec)
    local prefix = 'seconds'
    local result = 0
    if sec >= 60 then
        prefix = 'minutes'
        result = sec / 60
    end
    if result >= 60 then
        prefix = 'hours'
        result = result / 60
    end
    if result >= 24 then
        prefix = 'days'
        result = result / 24
    end
    return prefix, result
end
