function all_utils.table_to_string(t)
    local result = ''
    for key, value in pairs(t) do
        result = result .. tostring(key) .. ' = ' .. tostring(value) .. '\n'
    end
    return result:trim()
end

function all_utils.table_to_string_keys(t)
    local result = ''
    for key in pairs(t) do
        result = result .. tostring(key) .. ', '
    end
    return result:sub(0, result:len()-2)
end

function all_utils.table_to_string_values(t)
    local result = ''
    for _, value in pairs(t) do
        result = result .. tostring(value) .. ', '
    end
    return result:sub(0, result:len()-2)
end

function all_utils.table_len(t)
    local result = 0
    for key in pairs(t) do result = result + 1 end
    return result
end

function all_utils.table_floor(t)
    local result = {}
    for key, value in pairs(t) do result[key] = math.floor(value) end
    return result
end

function all_utils.table_equals(t, t2)
    for key, value in pairs(t) do
        if t2[key] and type(value) == 'table' and type(t2[key]) == 'table' then
            if not equal(value, t2[key]) then return false end
        elseif not t2[key] or t2[key] ~= value then return false end
    end
    return true
end

function all_utils.table_keys(t)
    local result = {}
    for key in pairs(t) do table.insert(t, key) end
    return result
end

function all_utils.table_values(t)
    local result = {}
    for _, value in pairs(t) do table.insert(t, value) end
    return result
end

function all_utils.table_split(t, num, named)
    local result = {}
    local i = 0
    local part = {}
    for key, value in pairs(t) do
        if named then part[key] = value
        else table.insert(part, value) end
        i = i + 1
        if i >= num then
            table.insert(result, part)
            i = 0
            part = {}
        end
    end
    return result
end
