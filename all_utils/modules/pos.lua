function all_utils.get_full_pos(player)
    local result = player:getpos()
    result.pitch = player:get_look_vertical()
    result.yaw = player:get_look_horizontal()
    return result
end

function all_utils.set_full_pos(player, pos)
    player:setpos(pos)
    player:set_look_vertical(pos.pitch)
    player:set_look_horizontal(pos.yaw)
end

function all_utils.is_empty(pos)
    local node = minetest.get_node_or_nil(pos)
    if node and node.name then
    	local def = minetest.registered_nodes[node.name]
    	if def and not def.walkable then
    		return true
    	end
    end
    return false
end

function all_utils.find_position(pos, posnear)
    local function get_near(num) -- return +/- num or 0 if over limit
        if num == 0 then return 1
        elseif num > 0 and num < posnear then return num + 1
        elseif num > 0 and num >= posnear then return -1
        elseif num < 0 and math.abs(num) < posnear then return num - 1
        elseif num < 0 and math.abs(num) >= posnear then return 0 end
    end

    local function get_pos(xyz, add)
        return {xyz.x + add.x, xyz.y + add.y, xyz.z + add.z}
    end

    local result = {x = 0, y = 0, z = 0}
    while true do
        result.x = get_near(result.x)
        if result.x == 0 then return false, pos end
        local xyz = get_pos(pos, result)
        if all_utils.is_empty(xyz) and not all_utils.is_empty({x = xyz.z, y = xyz.y - 1, z = xyz.z})
            then return true, xyz
        else
            while true do
                result.z = get_near(result.z)
                if result.z == 0 then
                    result.z = 0
                    break
                end
                xyz = get_pos(pos, result)
                if all_utils.is_empty(xyz) and not all_utils.is_empty({x = xyz.z, y = xyz.y - 1, z = xyz.z}) then
                    return true, xyz
                else
                    while true do
                        result.y = get_near(result.y)
                        if result.y == 0 then
                            result.y = 0
                            break
                        end
                        xyz = get_pos(pos, result)
                        if all_utils.is_empty(xyz) and not all_utils.is_empty({x = xyz.z, y = xyz.y - 1, z = xyz.z}) then
                            return true, xyz
                        end
                    end
                end
            end
        end
    end
end

function all_utils.find_top_or_down_pos(pos, top, limit)
    local y = 1
    if not top then y = -1 end
    local old_empty = all_utils.is_empty(pos)
    local old_xyz = nil
    while true do
        local xyz = {x = pos.x, y = pos.y + y, z = pos.z}
        if old_xyz then
            if top then
                if not old_empty and all_utils.is_empty(xyz) then return true, xyz end
            else
                if old_empty and not all_utils.is_empty(xyz) then return true, old_xyz end
            end
        end
        old_empty = all_utils.is_empty(xyz)
        old_xyz = xyz
        if top then y = y + 1 else y = y - 1 end
        if math.abs(y - pos.y) > limit then return false, pos end
    end
end
