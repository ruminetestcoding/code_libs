-- local functions
local function to_table(str)
    local array = {}
    local start = 1
    for i = 1, str:len() do
        if str:sub(i, i) == '\n' then
            table.insert(array, str:sub(start, i - 1))
            start = i + 1
        end
    end
    return array
end

-- API
function mod_configs.to_conf(data)
    local result = ''
    for name, section in pairs(data) do
        result = result .. '[' .. tostring(name) .. ']' .. '\n'
        for key, value in pairs(section) do
            result = result .. tostring(key) .. ' = ' .. tostring(value) .. '\n'
        end
        result = result .. '\n'
    end
    return result
end

function mod_configs.from_conf(str)
    if not str:find('\n') then return {} end
    local array = to_table(str)
    local result = {}
    local sec_name = nil
    for _, line in ipairs(array) do
        line = line:trim()
        if line:match('^%[(.+)%]$') then
            sec_name = line:sub(2, line:len() - 1)
            result[sec_name] = {}
        elseif line:len() >= 3 then
            local index = line:find('=')
            if index then
                local key = line:sub(1, index - 1):trim()
                local value = line:sub(index + 1, line:len()):trim()
                if value == 'true' then value = true
                elseif value == 'false' then value = false
                elseif tonumber(value) then value = tonumber(value) end
                result[sec_name][key] = value
            end
        end
    end
    return result
end
