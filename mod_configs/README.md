# mod_configs

This mod add easy support config files for mods. Formats:

* ini
* yaml
* json

## Depends

* fs_lib
* all_utils

## API

* **mod_configs.save_conf(dir, name, data)** - save ini config (dir - subdir name in conf dir, name - string key, data - table)
* **mod_configs.load_conf(dir, name)** - load ini config (dir - subdir name in conf dir, name - string key), return table or nil
* **mod_configs.save_yaml(dir, name, data)** - save yaml config (dir - subdir name in conf dir, name - string key)
* **mod_configs.load_yaml(dir, name)** - load yaml config (dir - subdir name in conf dir, name - string key), return table or nil
* **mod_configs.save_json(dir, name, data)** - save json config (dir - subdir name in conf dir, name - string key)
* **mod_configs.load_json(dir, name)** - load json config (dir - subdir name in conf dir, name - string key), return table or nil
* **mod_configs.get_path(mod)** - create and get current mod dir
* **mod_configs.to_conf(data)** - convert table to string ini config
* **mod_configs.from_conf(str)** - convert string ini config to table

### Yaml API

* **mod_configs.yaml.parse_yaml(str)** - parse yaml from string, return table
* **mod_configs.yaml.dump_yaml(t_yaml)** - dump yaml from table, return string
