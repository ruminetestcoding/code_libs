mod_configs = {}

local sep = fs_lib.separator()
local path = minetest.get_worldpath() .. sep .. 'configs'
local modpath = minetest.get_modpath(minetest.get_current_modname())

dofile(modpath .. sep .. 'conf.lua')
dofile(modpath .. sep .. 'yaml.lua')

local function create_dir(dir_path, subdir)
    if not fs_lib.exists(dir_path) then fs_lib.mkdir(dir_path) end
    local sd_path = dir_path .. sep .. subdir
    if not fs_lib.exists(sd_path) then fs_lib.mkdir(sd_path) end
end

-- [function] save ini config (dir - subdir name in conf dir, name - string key, data - table)
function mod_configs.save_conf(dir, name, data)
    create_dir(path, dir)
    local conf_path = path .. sep .. dir .. sep .. name .. '.conf'
    fs_lib.write_file(conf_path, mod_configs.to_conf(data))
end

-- [function] load ini config (dir - subdir name in conf dir, name - string key), return table or nil
function mod_configs.load_conf(dir, name)
    local conf_path = path .. sep .. dir .. sep .. name .. '.conf'
    if not fs_lib.exists(conf_path) then return nil end
    return mod_configs.from_conf(fs_lib.read_file(conf_path))
end

-- [function] save yaml config (dir - subdir name in conf dir, name - string key)
function mod_configs.save_yaml(dir, name, data)
    create_dir(path, dir)
    local yaml_path = path .. sep .. dir .. sep .. name .. '.yml'
    fs_lib.write(yaml_path, mod_configs.yaml.dump_yaml(data))
end

-- [function] load yaml config (dir - subdir name in conf dir, name - string key), return table or nil
function mod_configs.load_yaml(dir, name)
    local yaml_path = path .. sep .. dir .. sep .. name .. '.yml'
    if not fs_lib.exists(yaml_path) then return nil end
    return mod_configs.yaml.parse_yaml(fs_lib.read_file(yaml_path))
end

-- [function] save json config (dir - subdir name in conf dir, name - string key)
function mod_configs.save_json(dir, name, data)
    create_dir(path, dir)
    local json_path = path .. sep .. dir .. sep .. name .. '.json'
    fs_lib.write_file(json_path, minetest.write_json(data, true))
end

-- [function] load json config (dir - subdir name in conf dir, name - string key), return table or nil
function mod_configs.load_json(dir, name)
    local json_path = path .. sep .. dir .. sep .. name .. '.json'
    if not fs_lib.exists(json_path) then return nil end
    return minetest.parse_json(fs_lib.read_file(json_path))
end

-- [function] create and get current mod dir
function mod_configs.get_path(mod)
    create_dir(path, mod)
    return path .. sep .. mod
end
