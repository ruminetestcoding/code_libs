mod_configs.yaml = {}

local function uncomment(str)
	if str:find('#') then
		return str:sub(1, str:find('#') - 1):trim()
	end
	return str
end

local function unquotes(str)
	local q = str:sub(1, 1)
	if q ~= '\'' and q ~= '"' then return uncomment(str) end
	local escape = false
	local eind = nil
	for i = 2, str:len() do
		local c = str:sub(i, i)
		if c == '\\' then escape = true
		elseif c == q and not escape then
			eind = i
			break
		else
			escape = false
		end
	end
	if eind then return str:sub(2, eind - 1) end
	uncomment(str)
end

local function typecast(value)
	if tonumber(value) then value = tonumber(value)
	elseif value == 'true' then value = true
	elseif value == 'false' then value = false
	end
	return value
end

function mod_configs.yaml.parse_yaml(str)
	local str_list = str:split('\n')
	local result = {}
	local name = nil
	local array = {}
	
	local function save_array()
		if not name then return end
		result[name] = array
		name = nil
		array = {}
	end
	
	for _, line in ipairs(str_list) do
		local ch = line:sub(1, 1)
		if ch == '#' then
			ch = nil
		elseif ch == '-' then
			if name then
				table.insert(array, typecast(unquotes(line:sub(2, line:len()):trim())))
			end
		elseif line:find(':') then
			local ind = line:find(':')
			local key = line:sub(1, ind - 1):trim()
			if line:len() > ind then
				save_array()
				result[key] = typecast(unquotes(line:sub(ind + 1, line:len()):trim()))
			else
				save_array()
				name = key
			end
		end
	end
	save_array()
	return result
end

function mod_configs.yaml.dump_yaml(t_yaml)
	local result = ''
	for key, value in pairs(t_yaml) do
		if value ~= nil and type(value) ~= 'table' then
			result = result .. tostring(key) .. ': ' .. tostring(value) .. '\n'
		elseif type(value) == 'table' and all_utils.table_len(value) > 0 then
			result = result .. tostring(key) .. ':\n'
			for _, line in ipairs(value) do
				result = result .. '- ' .. tostring(line) .. '\n'
			end
		end
	end
	return result
end
