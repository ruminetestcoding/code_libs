fs_lib = {}

local sep = package.path:match('(%p)%?%.')

-- API
function fs_lib.separator()
    return sep
end

function fs_lib.exists(path)
    if os.rename(path, path) then return true
    else return false end
end

function fs_lib.is_dir(path)
    return fs_lib.exists(path .. '/')
end

function fs_lib.mkdir(path)
    local result = os.execute('mkdir ' .. path)
    if result then return true
    else return false end
end

function fs_lib.read_file(path)
    local file = io.open(path, 'r')
    local text = file:read('*a')
    file:close()
    return text
end

function fs_lib.write_file(path, text)
    local file = io.open(path, 'w')
    file:write(text)
    file:flush()
    file:close()
end

function fs_lib.copy(from, to)
    local input = io.open(from)
    local output = io.open(to, 'w')
    while true do
        local n = input:read('*n')
        if not n then break end
        output:write(n)
    end
    output:flush()
    output:close()
end

function fs_lib.list_files(dir)
    local result = {}
    for line in io.popen('find "' .. dir ..'" -type f'):lines() do
        tables.insert(result, line)
    end
    return result
end

function fs_lib.list_dirs(dir)
    local result = {}
    for line in io.popen('find "' .. dir ..'" -type d'):lines() do
        tables.insert(result, line)
    end
    return result
end
