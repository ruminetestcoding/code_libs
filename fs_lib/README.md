# fs_lib

API for file system.

* **fs_lib.separator()** - return path separator
* **fs_lib.exists(path)** - return *boolean*, check exists file or dir (path - *string*)
* **fs_lib.is_dir(path)** - return *boolean*, check exists dir
* **fs_lib.mkdir(path)** - return *boolean*, create dir
* **fs_lib.copy(from, to)** - copy file
* **fs_lib.read_file(path)** - return *string*, read file
* **fs_lib.write_file(path, text)** - write text to file (mode *w* - rewrite)
* **fs_lib.list_files(dir)** - return array *table* (only unix, usig find)
* **fs_lib.list_dirs(dir)** - return array *table* (only unix, using find)
